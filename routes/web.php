<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ChatController@index')->middleware('auth');
Route::get('/messages/{recipient}', 'ChatController@messages')->middleware('auth');
Route::post('/messages', 'ChatController@saveMessage')->middleware('auth');
Route::get('/recipient/{id}', 'ChatController@recipient')->middleware('auth');
Route::get('/users', 'ChatController@users')->middleware('auth');


Route::get('/test/index', 'testController@index');
Route::get('/test/test', 'testController@test');
Route::get('/test/ping', 'testController@ping');
Route::get('/test/getEvents', 'testController@getEvents');

Auth::routes();
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
