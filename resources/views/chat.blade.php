<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chat</title>
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script>
        $(function() {

            function ping() {
                $.get('/ping');
                $.get('/getEvents', function (data) {
                    console.log(JSON.parse(data));
                });
            }
            setInterval(ping, 5000);
        });
    </script>
</head>
<body>
<div>
    @foreach($contacts->contacts as $contact)
        {{ $contact->display_name }}<br>
    @endforeach
</div>
</body>
</html>