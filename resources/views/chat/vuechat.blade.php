@extends('layouts.app')

@section('content')
    <contacts :users="users" v-on:participantchanged="switchChat"></contacts>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div id="chat">
                    <div class="panel-heading"><div class="col-md-4">ChatRoom</div><chat-recipient :recipient="recipient"></chat-recipient></div>
                            <chat-log :messages="messages"></chat-log>
                            <chat-composer v-on:messagesent="addMessage" :recipient="recipient" :user="user"></chat-composer>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection