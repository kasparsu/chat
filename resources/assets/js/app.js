/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));
Vue.component('chat-recipient', require('./components/ChatRecipient.vue'));
Vue.component('contacts', require('./components/Contacts.vue'));
Vue.component('contact', require('./components/User.vue'));

const app = new Vue({
    el: '#app',
    methods: {
        addMessage(message) {
            this.messages.push(message);
            axios.post('/messages', {
                'from': message.user.id,
                'message': message.message,
                'to': message.recipient
            })
        },
        switchChat(participant) {
            this.recipient = participant;
            this.messages = [];
            axios.get('/messages/' + + this.recipient.id).then(response => {
                this.messages = response.data;
            });
        }
    },
    data() {
        return {
            messages: [],
            user: [],
            recipient: {
                    id: Math.abs(user.id - 3),
                    name: ''
            },
            usersActive: [],
            users: []
        }
    },
    created() {
        axios.get('/messages/' + + this.recipient.id).then(response => {
            this.messages = response.data;
        });
        axios.get('/recipient/' + this.recipient.id).then(response => {
            this.recipient = response.data;
        });
        axios.get('/users').then(response => {
            this.users = response.data;
        });
        this.user = window.user;

        Echo.join('chat.room')
            .here((users) => {
                this.usersActive = users;
                console.log(this.usersActive);
                console.log(users);
            })
            .joining((user) => {
                console.log(user);
            this.usersActive.push(user);
            console.log(this.usersActive);
            })
            .leaving((user) => {
                this.usersActive = this.usersActive.filter(u => u != user);
                console.log(this.usersActive);
            })
            .listen('MessagePosted', (e) => {
                this.messages.push(e.message);
            });
    }
});