<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'message', 'user', 'recipient', 'provider'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function recipient(){
        return $this->belongsTo(User::class);
    }
}
