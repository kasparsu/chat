<?php
/**
 * Created by PhpStorm.
 * User: kaspa
 * Date: 28.03.2017
 * Time: 23:05
 */

namespace App;


use GuzzleHttp\Client;

class SkypeApi
{
    public $conn;

    /**
     * SkypeApi constructor.
     * @param $conn
     */
    public function __construct(SkypeConnection $conn, Client $guzzle)
    {
        $this->conn = $conn;
        $this->guzzle = $guzzle;
    }

    public function sendMessage($msghost, $regToken, $message){
        $resp = $this->guzzle->post( $this->conn->msgshost ."/users/ME/conversations/8:trclin/messages", [
            "headers"=> ['Registrationtoken' => 'registrationToken='. $this->conn->regToken],
            "json"=>["content"=> $message,
                "messagetype"=>'RichText',
                'contenttype'=> 'text',
                'Mentions' => 'false',
                'imdisplayname'=> 'Kaspar Suursalu',
                'clientmessageid' => time() * 1000
            ]]);
        $html = $resp->getBody()->getContents();
        //var_dump($html);
    }
    public function config(){
        $resp = $this->guzzle->put( $this->conn->msgshost . '/users/ME/endpoints/' . $this->conn->endpoint . '/presenceDocs/messagingService', [
            "headers"=> ['Registrationtoken' => 'registrationToken='.$this->conn->regToken],
            "json"=>["id"=> 'messagingService',
                "type"=>'EndpointPresenceDoc',
                'selfLink'=> 'uri',
                'privateInfo' => ['epname'=> 'skype'],
                'publicInfo' => [
                    'capabilities'=> 'video|audio',
                    'type' => 1,
                    'skypeNameVersion' => 'skype.com',
                    'nodeInfo' => "",
                    'version' => "908/1.85.0.29//skype.com"
                ]]]);
        $html = $resp->getBody()->getContents();
        //var_dump(json_decode($html));
    }
    public function subscribe(){
        $resp = $this->guzzle->post($this->conn->msgshost . '/users/ME/endpoints/' . $this->conn->endpoint . '/subscriptions', [
            "headers"=> ['Registrationtoken' => 'registrationToken='.$this->conn->regToken],
            "json"=>['interestedResources' => [
                '/v1/threads/ALL',
                '/v1/users/ME/contacts/ALL',
                '/v1/users/ME/conversations/ALL/messages',
                '/v1/users/ME/conversations/ALL/properties'
            ],
                'template' => 'raw',
                'channelType' => 'httpLongPoll']]);
        $html = $resp->getBody()->getContents();
        //var_dump(json_decode($html));
    }
    public function getEvents(){
        $resp = $this->guzzle->post($this->conn->msgshost . '/users/ME/endpoints/' . $this->conn->endpoint . '/subscriptions/0/poll', [
            "headers"=> ['Registrationtoken' => 'registrationToken='.$this->conn->regToken]]);
        $html = $resp->getBody()->getContents();
        return $html;
    }
    public function getContacts(){
        $resp = $this->guzzle->get(SkypeConnection::API_CONTACTS . '/users/' . $this->conn->userId, ['query' => [
            'delta' => '',
            'reason' => "default"
        ],
            "headers"=> ['X-Skypetoken' => $this->conn->skypeToken]]);
        $html = $resp->getBody()->getContents();
        //var_dump($html));
        return json_decode($html);
    }
    public function ping(){
        $resp = $this->guzzle->post($this->conn->msgshost . '/users/ME/endpoints/' . $this->conn->endpoint . '/active', ["json"=> ['timeout' => 5], "headers"=> ['Registrationtoken' => 'registrationToken='.$this->conn->regToken]]);
        $html = $resp->getBody()->getContents();
        //var_dump(json_decode($html));
        return $html;
    }
}