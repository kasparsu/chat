<?php

namespace App\Console;

use App\SkypeApi;
use App\SkypeConnection;
use App\Token;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Console\Command;

class Ping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:ping';


    /**
     * Create a new command instance.
     */
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = 'kasparsu@gmail.com';
        $token = Token::where('username', $username)->first();
        $conn = new SkypeConnection($username, Null, new Client(), new CookieJar(), $token);
        $api = new SkypeApi($conn, new Client());
            while(true) {
                var_dump($api->ping());
                $api->getEvents();
                sleep(5);
            }
    }
}