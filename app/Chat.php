<?php
/**
 * Created by PhpStorm.
 * User: kaspa
 * Date: 23.05.2017
 * Time: 22:12
 */

namespace App;


use Illuminate\Database\Eloquent\Collection;

class Chat
{
    private $contacts;
    private $events;
    /**
     * Chat constructor.
     * @param $contacts
     */
    public function __construct()
    {
        $this->contacts = new Collection();
        $this->events = new Collection();
    }

    public function getContacts(){
        return $this->contacts;
    }
    public function addContact($id, $username, $displayName){
        $contact = new \stdClass();
        $contact->id = $id;
        $contact->username = $username;
        $contact->displayName = $displayName;
        $this->contacts->add($contact);
    }
}