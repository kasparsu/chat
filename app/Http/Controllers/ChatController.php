<?php
/**
 * Created by PhpStorm.
 * User: kaspa
 * Date: 26.05.2017
 * Time: 19:42
 */

namespace App\Http\Controllers;


use App\Events\MessagePosted;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index() {
        return view('chat.vuechat');
    }
    public function messages($recipient) {
        return Message::with([ 'user', 'recipient' ])->where(function($query) use($recipient) {
            $query->where('user_id', Auth::user()->id)
                ->where('recipient_id', $recipient);
        })->orWhere(function($query) use($recipient) {
            $query->where('user_id', $recipient)
                ->where('recipient_id', Auth::user()->id);
        })->get();
    }
    public function recipient($id) {
        return User::find($id);
    }
    public function saveMessage(Request $request) {

        $message = new Message();
        $message->user_id = $request->input('from');
        $message->message = $request->input('message');
        $message->recipient_id = $request->input('to');
        $message->save();
        $message->load('user', 'recipient');
        broadcast(new MessagePosted($message))->toOthers();

    }
    public function users() {
        return User::all()->except(Auth::user()->id);
    }
}