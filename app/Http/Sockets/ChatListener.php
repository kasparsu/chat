<?php namespace App\Http\Sockets;

use App\SkypeApi;
use App\SkypeConnection;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Orchid\Socket\BaseSocketListener;
use Ratchet\ConnectionInterface;
use React\EventLoop\LoopInterface;

class ChatListener extends BaseSocketListener {

 protected $clients;
    private $loop;
    public function __construct(LoopInterface $loop) {
        $this->clients = new \SplObjectStorage;
        $this->loop = $loop;

    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg) {

    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }

}