<?php
/**
 * Created by PhpStorm.
 * User: kaspa
 * Date: 23.05.2017
 * Time: 22:16
 */

namespace App;


use App\SkypeApi;

class SkypeToChat
{
    private $api;
    private $chat;

    /**
     * SkypeToChat constructor.
     */
    public function __construct(SkypeApi $skypeApi, Chat $chat)
    {
        $this->api = $skypeApi;
        $this->chat = $chat;
        $this->init();
    }
    private function init(){
        $this->contacts();
    }
    public function contacts(){
        $contacts = $this->api->getContacts();
        foreach($contacts->contacts as $contact){
            $this->chat->addContact($contact->person_id, $contact->mri, $contact->display_name);
        }
    }
    public function events(){
        $events = $this->api->getEvents();
//        foreach($contacts->contacts as $contact){
//            $this->chat->addEvent($event->type, $contact->mri, $contact->display_name);
//        }
    }
}