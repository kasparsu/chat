<?php

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Parent command namespace.
     *
     * @var string
     */
    protected $namespace = 'App\\Console\\';

    /**
     * The available command shortname.
     *
     * @var array
     */
    protected $commands = [
        'ping',
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->commands as $command) {
            $this->commands($this->namespace.$command);
        }
        $this->app->singleton('GuzzleHttp\Client', function () {
            return new \GuzzleHttp\Client();
        });
        $this->app->bind('GuzzleHttp\Cookie\CookieJar', function () {
            return new \GuzzleHttp\Cookie\CookieJar();
        });
        $this->app->bind('GuzzleHttp\Cookie\SetCookie', function () {
            return new \GuzzleHttp\Cookie\SetCookie();
        });
        $this->app->resolving(CookieJar::class, function () {
            //var_dump('resolved Guzzler');
        });
    }
    /**
     * @return array
     */
    public function provides()
    {
        $provides = [];

        foreach ($this->commands as $command) {
            $provides[] = $this->namespace.$command;
        }

        return $provides;
    }
}
