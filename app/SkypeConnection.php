<?php
/**
 * Created by PhpStorm.
 * User: kaspa
 * Date: 27.03.2017
 * Time: 22:07
 */

namespace App;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Symfony\Component\DomCrawler\Crawler;

class SkypeConnection
{
    const API_MSACC = 'https://login.live.com';
    const API_LOGIN = 'https://login.skype.com/login';
    const API_USER = 'https://api.skype.com';
    const API_JOIN = 'https://join.skype.com';
    const API_BOT = 'https://api.aps.skype.com/v1';
    const API_FLAGS = 'https://flagsapi.skype.com/flags/v1';
    const API_ENTITLEMENT = 'https://consumer.entitlement.skype.com';
    const API_SCHEDULE = 'https://api.scheduler.skype.com';
    const API_TRANSLATE = 'https://dev.microsofttranslator.com/api';
    const API_URL  = 'https://urlp.asm.skype.com/v1/url/info';
    const API_CONTACTS  = 'https://contacts.skype.com/contacts/v2';
    const API_MSGSHOST  = 'https://client-s.gateway.messenger.live.com/v1';
    const API_PEOPLE  = 'https://people.directory.live.com/people/account/settings';
    const API_DIRECTORY  = 'https://skypegraph.skype.com/search/v1.1/namesearch/swx/';
    const API_PROFILE  = 'https://pf.directory.live.com/profile/mine/System.ShortCircuitProfile.json';
    const API_CONFIG   = 'https://a.config.skype.com/config/v1';

    private $username;
    private $password;
    /** @var Client */
    private $guzzle;
    /** @var CookieJar */
    private $jar;
    public $skypeToken;
    public $regToken;
    public $endpoint;
    public $skypeTokenExpiry;
    public $regTokenExpiry;
    public $msgshost;
    private $PPFT;
    private $MSPOK;
    private $MSPRequ;
    public $userId;
    /**
     * SkypeConnection constructor.
     */
    public function __construct($username, $password = null, Client $guzzle, CookieJar $jar, Token $token = NULL)
    {
        $this->username = $username;
        $this->password = $password;
        $this->guzzle = $guzzle;
        $this->jar = $jar;
        if($token == null) {
        $this->init();
            $this->save();
        } else {
            $this->username = $token->username;
            $this->skypeToken=  $token->token;
            $this->regToken = $token->regtoken;
            $this->skypeTokenExpiry = $token->skypeExpiry;
            $this->regTokenExpiry = $token->regExpiry;
            $this->msgshost = $token->msgsHost;
            $this->endpoint = $token->endpoint;
            $this->userId = $token->userId;
        }
    }

    private function init(){

            $this->getParams();
            $t = $this->sendCreds();
            $this->getToken($t);
            $this->auth();
            $this->getUserId();
//        $this->config($regTokenInfo['msgshost'], $regTokenInfo['endpoint'], $regTokenInfo['regToken']);
//        $this->subscribe($regTokenInfo['msgshost'], $regTokenInfo['endpoint'], $regTokenInfo['regToken']);
    }
    private function save(){
        $token = new Token();
        $token->username = $this->username;
        $token->token = $this->skypeToken;
        $token->regtoken = $this->regToken;
        $token->skypeExpiry = $this->skypeTokenExpiry;
        $token->regExpiry = $this->regTokenExpiry;
        $token->msgsHost = $this->msgshost;
        $token->endpoint = $this->endpoint;
        $token->userId = $this->userId;
        $token->save();
    }
    private function getUserId(){
        $resp = $this->guzzle->get(SkypeConnection::API_USER . "/users/self/profile", ["headers"=> ['X-Skypetoken' => $this->skypeToken]]);
        $json = json_decode($resp->getBody()->getContents());
        $this->userId = $json->username;
    }
    private function checkUser(){

        $resp = $this->guzzle->post(SkypeConnection::API_MSACC . "/GetCredentialType.srf", ["json"=>["username"=> $this->username]]);
        $json = json_decode($resp->getBody()->getContents());
        if($json->Credentials->HasPassword){
            return true;
        }
        return false;
    }
    private function getParams(){
        $jar = $this->jar;
        $resp = $this->guzzle->get(SkypeConnection::API_LOGIN . "/oauth/microsoft", ['query' => [
            'client_id' => '578134',
            'redirect_uri' => "https://web.skype.com"
        ], 'cookies' => $jar]);
        $html = $resp->getBody()->getContents();
        $extract = substr($html,strpos($html,'name="PPFT"'),500);
        $start = strpos($extract, 'value="') + 7;
        $end = strpos($extract, '"/>');
        $this->PPFT = substr($extract, $start , $end-$start);
        $jarArray = $jar->toArray();
        $nameArray = array_column($jarArray, 'Name');
        $MSPRequKeyPos = array_search('MSPRequ', $nameArray);
        $MSPOKKeyPos = array_search('MSPOK', $nameArray);
         $this->MSPRequ = $jarArray[$MSPRequKeyPos];
         $this->MSPOK = $jarArray[$MSPOKKeyPos];

        return true;
    }
    private function sendCreds(){

        $newCookie = new SetCookie($this->MSPRequ);
        $jar = $this->jar;
        $jar->setCookie($newCookie);
        $newCookie = new SetCookie($this->MSPOK);
        $jar->setCookie($newCookie);
        $ckTstCookieArray = $this->MSPOK;
        $ckTstCookieArray['Name'] = 'CkTst';
        $ckTstCookieArray['Value'] = "" . time()*1000;
        $newCookie = new SetCookie($ckTstCookieArray);
        $jar->setCookie($newCookie);
        $resp = $this->guzzle->post(SkypeConnection::API_MSACC . "/ppsecure/post.srf", [
            'query' => [
                'wa' => 'wsignin1.0',
                'wp'     => 'MBI_SSL',
                'wreply'      => 'https://lw.skype.com/login/oauth/proxy?client_id=578134&site_name=lw.skype.com&redirect_uri=https%3A%2F%2Fweb.skype.com%2F'
            ],
            'cookies' => $jar,
            'form_params' => [
                'login' => $this->username,
                'passwd' => $this->password,
                'PPFT' => $this->PPFT
            ]]);

        $html = $resp->getBody()->getContents();
        $parser = new Crawler($html);
        $t = $parser->filter("input#t")->getNode(0)->getAttribute("value");
        return $t;
    }
    private function getToken($t){
        $resp = $this->guzzle->post(SkypeConnection::API_LOGIN . "/microsoft", ['query' => [
            'client_id' => '578134',
            'redirect_uri' => "https://web.skype.com"
        ], 'form_params' => [
            't' => $t,
            'client_id' => '578134',
            'oauthPartner' => '999',
            'site_name' => 'lw.skype.com',
            'redirect_uri' => 'https://web.skype.com'
        ]]);

        $html = $resp->getBody()->getContents();
        $parser = new Crawler($html);
        $this->skypeToken = $parser->filter("input[name='skypetoken']")->getNode(0)->getAttribute("value");
        $this->skypeTokenExpiry = time() + (int) $parser->filter("input[name='expires_in']")->getNode(0)->getAttribute("value");
        return true;
    }
    private function auth(){
        $regToken = null;
        $expiry = null;
        $endpoint = null;
        $msgshost = SkypeConnection::API_MSGSHOST;
        while(!isset($this->regToken)) {
            $secs = time();
            $hash = $this->getMac256Hash($secs);
//            $stack = new HandlerStack();
//            $stack->setHandler(new CurlHandler());
//            $client = new Client(['handler' => $stack]);
            $resp = $this->guzzle->post('https://client-s.gateway.messenger.live.com/v1/users/ME/endpoints', ['json' => ['endpointFeatures' => 'Agent'],
                'headers' => ['Lockandkey' => "appId=msmsgs@msnmsgr.com; time=$secs; lockAndKeyResponse=$hash",
                    'Authentication' => "skypetoken=$this->skypeToken"
                ]
            ]);
            $locationHeader = $resp->getHeader('Location');
            $regTokenHeader = $resp->getHeader('Set-RegistrationToken');
            if (!empty($regTokenHeader)) {
                $regTokenParts = explode(';',$regTokenHeader[0]);
                $this->regToken = substr($regTokenParts[0], strpos($regTokenParts[0], '=') +1, strlen($regTokenParts[0]));
                $this->regTokenExpiry = substr($regTokenParts[1], strpos($regTokenParts[1], '=') +1, strlen($regTokenParts[1]));
                $this->endpoint = substr($regTokenParts[2], strpos($regTokenParts[2], '=') +1, strlen($regTokenParts[2]));
            }
            if (!empty($locationHeader)) {
                $splitArray[0] = substr($locationHeader[0], 0, strpos($locationHeader[0], '/users'));
                $splitArray[2] = substr($locationHeader[0], strpos($locationHeader[0], 'endpoints/') + 10, strlen($locationHeader[0]));
                $splitArray[1] = '/' . $splitArray[2];
                if ($msgshost != $splitArray[0]) {
                    $this->msgshost = $splitArray[0];
                }
            }
        }
        return true;
    }
    private function getMac256Hash($challenge, $appId="msmsgs@msnmsgr.com", $key="Q1P7W2E4J9R8U3S5"){
        $clearText = $challenge . $appId;
        $clearText .= str_repeat("0", (8 - strlen($clearText) % 8));
        $cchClearText = intval(floor(strlen($clearText) / 4));
        $pClearText = [];
        for($i=0; $i<$cchClearText; $i++) {
            $pClearText[$i] = "0";
            for($pos=0; $pos<4; $pos++) {
                $pClearText[$i] += ord($clearText[4 * $i + $pos]) * (256 ** $pos);
            }
        }
        $sha256Hash = [0, 0, 0, 0];
        $hash = strtoupper(hash('sha256', $challenge . $key));
        for($i=0; $i<count($sha256Hash); $i++) {
            $sha256Hash[$i] = 0;
            for($pos=0; $pos<4; $pos++) {
                $dpos = 8 * $i + $pos * 2;
                $sha256Hash[$i] += hexdec(substr($hash, $dpos, 2)) * (256 ** $pos);
            }
        }
        $macHash = $this->cS64($pClearText, $sha256Hash);
        $macParts = [$macHash[0], $macHash[1], $macHash[0], $macHash[1]];
        $result = array_map(array($this, 'int64Xor'),$sha256Hash, $macParts);
        $result = implode("",array_map(array($this, 'int32ToHexString'), $result));
        return $result;
    }
    private function int32ToHexString($n){
        $hexChars = "0123456789abcdef";
        $hexString = "";
        for($i=0; $i<4; $i++){
            $hexString .= $hexChars[($n >> ($i * 8 + 4)) & 15];
            $hexString .= $hexChars[($n >> ($i * 8)) & 15];
        }
        return $hexString;
    }
    private function int64Xor($a, $b){
        $sA = decbin($a);
        $sB = decbin($b);

        $sC = "";
        $sD = "";
        $diff = abs(strlen($sA) - strlen($sB));
        for($i=0; $i<$diff; $i++){
            $sD .= "0";
        }
        if (strlen($sA) < strlen($sB)){
            $sD .= $sA;
            $sA = $sD;
        } elseif(strlen($sB) < strlen($sA)){
            $sD .= $sB;
            $sB = $sD;
        }
        for($i=0; $i<strlen($sA); $i++){
            $sC .= "" . intval($sA[$i] != $sB[$i]);
        }
        return bindec($sC);
    }
    private function cS64($pdwData, $pInHash){
        $MODULUS = 2147483647;
        $CS64_a = $pInHash[0] & $MODULUS;
        $CS64_b = $pInHash[1] & $MODULUS;
        $CS64_c = $pInHash[2] & $MODULUS;
        $CS64_d = $pInHash[3] & $MODULUS;
        $CS64_e = 242854337;
        $pos = 0;
        $qwDatum = 0;
        $qwMAC = 0;
        $qwSum = 0;
        for($i=0; $i<intval(floor(count($pdwData)/2)); $i++){
            $qwDatum = $pdwData[$pos];
            $pos += 1;
            $qwDatum = bcmul($qwDatum, $CS64_e );
            $qwDatum = bcmod($qwDatum, $MODULUS);
            $qwMAC = bcadd($qwMAC,$qwDatum);
            $qwMAC = bcmul($qwMAC, $CS64_a);
            $qwMAC = bcadd($qwMAC, $CS64_b);
            $qwMAC = bcmod($qwMAC, $MODULUS);
            $qwSum = bcadd($qwSum,$qwMAC);
            $qwMAC = bcadd($qwMAC, $pdwData[$pos]);
            $pos += 1;
            $qwMAC = bcmul($qwMAC, $CS64_c);
            $qwMAC = bcadd($qwMAC, $CS64_d);
            $qwMAC = bcmod($qwMAC, $MODULUS);
            $qwSum = bcadd($qwSum, $qwMAC);
        }
        $qwMAC = bcadd($qwMAC, $CS64_b);
        $qwMAC = bcmod($qwMAC, $MODULUS);
        $qwSum = bcadd($qwSum, $CS64_d);
        $qwSum = bcmod($qwSum, $MODULUS);
        return [intval($qwMAC), intval($qwSum)];
    }

}