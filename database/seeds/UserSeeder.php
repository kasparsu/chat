<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['Kaspar', 'kasparsu@gmail.com', 'Teerada1'],
            ['Martin', 'martin@gmail.com', 'Teerada1'],
            ['Teder', 'teder@gmail.com', 'Teerada1']
        ];
        foreach ($users as $user){
            $model = new \App\User();
            $model->name = $user[0];
            $model->email = $user[1];
            $model->password = bcrypt($user[2]);
            $model->save();
        }
    }
}
