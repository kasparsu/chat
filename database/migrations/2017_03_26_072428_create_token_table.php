<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->text('token')->nullable();
            $table->string('username');
            $table->string('userId');
            $table->bigInteger('skypeExpiry')->nullable();
            $table->longText('regtoken')->nullable();
            $table->bigInteger('regExpiry')->nullable();
            $table->string('msgsHost')->nullable();
            $table->string('endpoint')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokens');
    }
}
